def declare_variables(variables, macro):
    """
    This is the hook for the functions

    - variables: the dictionary that contains the variables
    - macro: a decorator function, to declare a macro.
    """

    # ex: {{ advancedSwitch('id-name--1', 'Activer les conseils avancés') }}
    @macro
    def advancedSwitch(id, label):
        "Add a toggle switch"
        HTML = """<input type='checkbox' id="%s" class='switch-input'><label for="%s" class='switch-label'>%s</label>"""
        return HTML % (id, id, label)