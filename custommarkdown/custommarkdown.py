import re
from mkdocs.plugins import BasePlugin

class CustomMarkdown(BasePlugin):
    config_scheme = ()

    # Will be trigered before markdown is interpretated
    def on_page_content(self, html, page, config, site_navigation=None, **kwargs):    
    # def on_page_markdown(self, markdown, page, config, site_navigation=None, **kwargs):

        # First find the advanced box text and add a closing div
        advancedBox = re.finditer(r"((?<=\+{3}\n)(?:\s{4}.*\n|\t.*\n)+)", html, flags=re.MULTILINE)

        for element in advancedBox:
            html = html.replace(element[0], element[0] + '</div>')
            # Respect paragraph (autoclose like magic)
            html = html.replace(element[0], re.sub(r"\t|\s{4}", '<p>', element[0], flags=re.IGNORECASE))

        # Then replace +++ by a div tag with advancedBox class
        html = html.replace("+++", '<div class="advancedBox">')
        
        return html