# Accueil


Vous trouverez ici différents guides pour apprendre à utiliser **Solus** et à y contribuer.

!!! Note
    Cette documentation est en cours de construction et n'est pas complète. Si vous souhaitez nous aider à l'améliorer rejoignez-nous [ici](https://framateam.org/signup_user_complete/?id=6j8q3or16jrajpyongfptuzpfy) ou [ici](@solus_fr).

---

![](https://getsol.us/imgs/solus.png)


## Qu'est-ce que Solus ?

**Solus** est un système d'exploitation, Windows et MacOS en sont également. Il permet de faire fonctionner votre ordinateur. Solus convient aussi bien aux débutants qu'aux utilisateurs avancés.

## Que puis-je faire avec Solus ?

Vous pouvez tout faire avec Solus, il convient parfaitement à une multitudes d'activités. Vous pouvez : 

 - **jouer** : Steam, [Lutris](lutris.net/)...
 - **faire de la bureautique** : [LibreOffice](https://fr.libreoffice.org/), WPS Office Suite, GnuCash...
 - **vous organiser** : contacts, calendrier, maps...
 - **communiquer** : email, Telegram, Discord, Mattermost, Bitmessage, Jitsi, Skype...
 - **écouter votre musique** : Spotify, Lollypop...
 - **regarder vos vidéos, films ou séries** : Netflix (via un navigateur web), VLC, Gnome MPV, Youtube...
 - **naviguer sur le Web** : Firefox, Brave, Chrome, Gnome Web, Falkon, Midori, Opera, Vivaldi...
 - **retoucher vos photos** : Gimp, Darktable...
 - **dessiner** : Inkscape...
 - **faire du montage vidéo et audio** : Kdenlive, Pitivi, PulseEffects...
 - **et bien d'autres choses encore, vous n'aurez pas assez d'une vie pour tout explorer !**

## Quels sont les avantages de Solus ?

Pourquoi utiliser Solus plutôt que Windows, MacOS ou une autre distribution Linux ? Eh bien Solus dispose de nombreux avantages.

 - **Stabilité** : Solus est très stable, vous n'aurez jamais de bugs, tout fonctionnera sans problèmes. Il est inpensable de voir disparaître ses fichiers personnelles après une mise à jour par exemple (coucou Windows).
 - **Simplicité** : vous êtes un débutant ? Pas de problèmes nous l'avons tous été un jour. Solus est simple à utiliser, l'interface est claire et intuitive, tout est en Français. Si vous êtes tout de même perdus une communauté sympathique sera là pour vous aider : [groupe Telegram Solus-fr](https://t.me/solusfr).
 - **Fraicheur** : sur Solus vous disposez des dernières mises à jour de vos applications préférées !
 - **Puissance** : Solus est simple à utiliser mais n'en est pas moins complète, vous y trouverez tous les logiciels et fonctionnalités dont vous aurez besoin.
 - **Légèreté** : Solus est léger, parfait pour ressuciter un vieil ordinateur ou pour améliorer les performances d'un plus récent.
 - **Rapidité** : sur Solus tout est rapide, installer une application, démarrer, éteindre, mettre en veille, sortir de veille, lancer une application, mettre à jour, etc. Absolument tout va vite, vous gagnerez un temps fous.
 - **Liberté** : Solus est un système d'exploitation [libre et open source](https://www.commentcamarche.com/faq/4626-mythe-logiciel-libre-logiciel-open-source). Pendant que Windows et autres vous espionnent Solus vous laisse tranquille et ne pille pas vos données personnelles. Il vous laisse également le choix sur tout, sur Solus vous êtes chez vous, c'est vous le patron et vous faites ce que vous voulez. Pas envie de faire la mise à jour maintenant : pas de problème c'est vous qui décidez ; vous voulez désinstaller ça, ça et ça :  à vos ordres. Vous êtes ***libre*** !
 - **Personnalité** : vous n'aimez pas être comme tout le monde ? Ça tombe bien, Solus est ultra-personnalisable. Vous pouvez faire quasiment tout ce que vous voulez. Du fond d'écran au thème graphique des fenêtres en passant par la position du panneau : vous pouvez tout changer. Avec Solus vous pouvez avoir quelque chose d'unique, finit les ordinateurs qui se ressemblent tous, place à l'**originalité** !


