# Changer de shell

Sur Solus, le shell Unix par défaut est Bash mais il est possible de le changer. Voici les différents shell disponibles dans les dépôts de Solus :

 - [Bash](https://www.gnu.org/software/bash/)
 - [Dash](https://www.in-ulm.de/~mascheck/various/ash/)
 - [Fish](https://fishshell.com/)
 - [Tcsh](https://fr.wikipedia.org/wiki/Tcsh)
 - [Zsh](https://www.zsh.org/)

## Installation

Pour utiliser un autre shell que Bash ou Sh (les deux sont déjà installés), il vous faudra installer le paquet correspondant. Pour cela il y a deux méthodes :

 - en utilisant le teminal il faut lancer la commande `sudo eopkg it <nom du shell>` ;
 - en utilisant le **Software Center** dans la catégorie **System utilities** qui se trouve dans **System Softwares** ou en cherchant directement.

!!! Note
    Il ne faut pas mettre de majuscules dans le nom des shells et il faut remplacer `<nom de shell>` par `zsh` par exemple et donc ne pas mettre les chevrons.

## Appliquer le changement

Maintenant que le shell est installé il vous reste à dire à Solus quel shell vous voulez utiliser. Il vous suffit d'ouvrir un terminal et d'y entrer `chsh -s $(which <nom du shell>)`.

## Exemple

Si vous voulez installer zsh par exemple, il ne vous faudra utliser que deux commandes que l'on peut mettre en une ligne comme suit :
`sudo eopkg it zsh && chsh -s $(which zsh)` (le `&&` permet de lancer la seconde commande si la première n'a rencontré aucune erreur).

!!! tip "Conseil"
    Je vous invite bien évidemment à chercher ce que font les commandes utilisées ici en lisant le manuel avec `man which` ou l'aide avec `eopkg it --help` par exemple. **Il est déconseillé d'exécuter des commandes qu'on ne comprend pas**.

**Source** : [Changing Shell](https://getsol.us/articles/configuration/changing-shell/en/)