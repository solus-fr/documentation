# Gestion du démarrage

Cet article va vous montrer différentes manières de gérer le démarrage du système.

## Afficher le menu de démarrage sans avoir paramétré de pause

Par défaut, les installations EFI ne montreront pas le menu de démarrage et lancerons directement Solus. Pour le faire apparaître il faut appuyer à répétition sur la touche ESPACE pendant le démarrage, cela peut prendre plusieurs essais.

## Ajouter des paramètres noyau au démarrage de Solus

On peut ajouter des paramètres noyau en créant un fichier pour que _clr-boot-manager_ l'utilise en mettant a jour le noyau. Par exemple pour utiliser _nomodeset_ il suffit d'utiliser

```sh
echo 'nomodeset' | sudo tee /etc/kernel/cmdline
```

Pour utiliser plusieurs paramètres il faudra les mettre sur une ligne et séparés par des espaces. Pour rendre effectifs les changements il faut ensuite utiliser `sudo clr-boot-manager-update`

## Afficher le menu de démarrage Solus par défaut

La commande suivante permet de changer le temps pendant lequel le menu est affiché au démarrage :

```sh
sudo clr-boot-manager set-timeout 5 && sudo clr-boot-manager update
```