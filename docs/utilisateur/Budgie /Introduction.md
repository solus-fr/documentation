# Introduction
[Budgie](https://getsol.us/solus/experiences/) est l'[environnement de bureau](https://fr.wikipedia.org/wiki/Environnement_de_bureau) par défaut sur Solus.
![Capture d'écran de Budgie](https://getsol.us//imgs//release-images/4.0/Budgie.jpg)