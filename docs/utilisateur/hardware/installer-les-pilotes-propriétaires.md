# Installer les pilotes propriétaires

Il peut être utile d'installer les pilotes propriétaires pour sa carte graphique pour améliorer les performances de la machine. Cependant ce n'est pas forcément utile pour une utilisation classique et l'autonomie de la batterie des ordinateurs portables est significativement réduite.

Pour lancer doFlicky, le logiciel d'installation des pilotes qui est pré-installé sur Solus, il y a deux manières :

 - en lançant la commande `doflicky-ui` dans un terminal ;
 - en lançant l'application **Hardware Drivers** (le logiciel n'est pas encore traduit) depuis votre menu.

Vous devriez donc vous retrouver avec la fenêtre suivante :

![Fenêtre doFlicky](https://framagit.org/solus-fr/documentation/raw/master/docs/utilisateur/hardware/Fen%C3%AAtre_doFlicky.png)

Il vous reste donc à choisir le pilote à installer dans le cadre sous la description et à cliquer sur **Install**. 

!!! Note
    Il faut aussi cocher la case pour installer le pilote 32-bit si vous allez utiliser Steam ou Wine.

Il ne vous reste plus qu'à redémarrer votre machine pour que le nouveau pilote soit actif.

**Source**: [Proprietary Drivers](https://getsol.us/articles/hardware/proprietary-drivers/en/)