# Installer et configurer LAMP (httpd)

Installer httpd (Apache) et mariadb sur Solus est relativement simple. Cependant la configuration de ces deux outils peut s'avérer plus complexe. Nous allons donc voir ici comment les configurer.

## 1 - httpd

### a - Installation

Commençons par installer httpd. Cette installation peut se faire via le gestionnaire de paquets eopkg ou via le software center.
Pour une installation en ligne de commande, tapez `sudo eopkg it httpd`.

Par défaut, le répertoire racine pour mettre vos sites web sera `/var/www`.

### b - Utilisation

Solus étant un système d'exploitation *stateless*  - c'est à dire qu'il peut opérer sans aucune customisation - il possède une configuration par défaut qui se trouve dans le répertoire `/usr/share/defaults/httpd`. Ce fichier ne doit pas être modifié au risque que nos changements soient écrasés lors d'une mise à jour de httpd.

Toutes nos modifications se feront donc dans le répertoire `/etc`. Commençons par créer un répertoire pour stocker nos configurations.

`sudo mkdir -p /etc/httpd/conf.d/`

Une fois le répertoire créé, nous allons créer notre fichier de configuration. 

`sudo touch /etc/httpd/conf.d/00-config.conf`

Toutes les configurations que vous voudrez ajouter à votre serveur seront mises dans ce fichier.

## 2 - PHP

### a - Installation

L'installation de php se fait également grâce à eopkg ou via le software center.
En ligne de commande : `sudo eopkg it php`.

Afin d'activer PHP via le module FPM et FastCGI vous devez créer un fichier de configuration nommé `php.conf` dans le répertoire `/etc/httpd/conf.d/`.

```
# /etc/httpd/conf.d/php.conf
LoadModule proxy_module lib64/httpd/mod_proxy.so
LoadModule proxy_fcgi_module lib64/httpd/mod_proxy_fcgi.so
<FilesMatch \.php$>
	SetHandler "proxy:fcgi://127.0.0.1:9000"
</FilesMatch>
<IfModule dir_module>
	DirectoryIndex index.php index.html
</IfModule>
```

Sauvegardez puis redémarrez httpd et le module fpm : `sudo systemctl restart httpd && sudo systemctl restart php-fpm`.

### b - Gestion

- Démarrer automatiquement httpd : `sudo systemctl enable httpd`
- Démarrer le serveur : `sudo systemctl start httpd`
- Arrêter le serveur : `sudo systemctl stop httpd`

## 3 - Mariadb

### a - Installation

Comme pour httpd et php, vous pouvez installer mariadb via le software center ou bien en ligne de commande. Pour cela tapez `sudo eopkg install mariadb-server` dans votre terminal. 
Vous pouvez ensuite choisir de démarrer automatiquement mariadb via la commande `sudo systemctl enable mariadb`. Enfin pour le démarrer, tapez `sudo systemctl start mariadb`.

### b - configuration

Vous pouvez à présent définir un mot de passe pour votre utilisateur root afin d'accéder à vos bases. Pour cela connectez vous à votre serveur mariadb via la commande suivante.
`mysql -u root -p`

Après vous êtres connecté, exécutez ces commandes afin de modifier le mot de passe du compte root. Replacez `votremotdepasse` par un mot de passe de votre choix.

```SQL
UPDATE mysql.user SET password = PASSWORD('votremotdepasse') WHERE user LIKE 'root';
FLUSH PRIVILEGES;
EXIT;
```

Puis connectez vous à votre serveur : `mysql -u root -p`. Saisissez le mot de passe rentré précédemment lorsque celui-ci vous est demandé. Si votre serveur est accessible depuis Internet vous devriez également désactiver les comptes anonymes et la base de test.

```SQL
DROP DATABASE test;
DELETE FROM mysql.user WHERE user = '';
FLUSH PRIVILEGES;
```

## 4 - phpmyadmin

Phpmyadmin n'est pour le moment pas disponible dans le dépôt officiel de Solus. Cependant l'installation est relativement simple. 
Pour cela placez vous dans le répertoire `/tmp` puis faites `wget https://files.phpmyadmin.net/phpMyAdmin/4.8.3/phpMyAdmin-4.8.3-all-languages.tar.gz`.
Une fois l'archive récupérée, décompressez la avec la commande `tar -zxvf phpMyAdmin-4.8.3-all-languages.tar.gz`. 
Enfin, vous pouvez copier le répertoire obtenu dans le répertoire `/var/www` avec la commande `sudo cp -r phpMyAdmin-4.8.3-all-languages /var/www/phpmyadmin`.

Vous pouvez désormais accéder à votre installation de phmyadmin à l'adresse `http://localhost/phpmyadmin`.

## 5 - Aller plus loin

Pour aller plus loin, vous pouvez également configurer des virtualhosts pour accéder à vos projets.

## 6 - Références

- https://www.stuartellis.name/articles/solus-setup/
- https://getsol.us/forums/viewtopic.php?t=7440
- https://getsol.us/articles/software/httpd/en/