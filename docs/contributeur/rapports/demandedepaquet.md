# Demander un paquet

---

Un logiciel dont vous avez besoin n'est pas présent dans le dépôt officiel de Solus ? Faites-en la demande !

# Première étape

Rendez vous sur [dev.getsol.us](https//dev.getsol.us/) puis cliquez dans le menu à gauche sur **Request New Package**. Il vous faudra remplir un formulaire, nous allons voir comment le faire de manière correcte.

# Élaboration de la demande

Indiquer dans la case _Title_, le nom du logiciel (exemple: Firefox).

Dans la description, il faudra indiquer **en anglais**, plusieurs informations:

- le nom de l'application
- l'adresse du site internet de l'application
- Indiquer pourquoi cette application devrait être inclue dans le dépôt officiel de Solus. En particulier, si une application ayant des fonctions similaires, il faudra indiquer les différences qu'offre cette nouvelle application.
- Indiquer si c'est un projet Open Source. Si laisser le tag en bas ** Package Request**, dans le cas contraire, retire-le et saisissez à la place **Third Party Repository**
- Indiquer un lien vers la source, c'est à dire le fichier compressé de l'application (tar ou zip). Ce fichier doit impérativement dans son nom, une numérotation pour signaler le numéro de version.

Une fois tout ces données rensignées, vous n'avez plus qu'à valider votre demande en cliquant sur **Create New Task**..

# Politique d'inclusion

L'équipe de Solus a une politique ferme sur l'inclusion de nouveaux paquets que vous devez prendre en considération. Votre demande risque d'être rejetée si:

- Le projet n'est plus actif.
- La licence du projet ne permet pas de redistribuer sous forme de paquets.
- Si le projet n'apporte aucune valeure ajoutée à Solus (paquet similaire déjà présent dans le dépôt).
- Si le projet est trop complexe et demanderait un mainteneur dédié. Ne pas demander par exemple de nouvel environnement de bureau.
- Si le projet ne fournit pas de source mais juste des binaires.
- Si le projet concerne un logiciel pour serveur web, Solus n'est pas dédié à être un OS pour serveur.
