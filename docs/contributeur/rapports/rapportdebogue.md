# Rapport de bogue

---

## Élaboration de votre rapport

Rendez vous sur [dev.getsol.us](https//dev.getsol.us/) puis cliquez sur **Report a new issue**.

Le rapport devrai être rédigé **en anglais**.

- Mettez en titre (title), un résumé de votre problème

- Dans description, mettez le plus détail sur le problème.

Enfin cliquez sur **Create New Task**.
