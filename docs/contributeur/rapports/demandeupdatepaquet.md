# Demander la mise à jour d'un paquet

---

Les sources de votre logiciel préféré a été mis à jour mais le paquet sur Solus est toujours à la version précédente. Il est possible que les développeurs passent à coté. Vous pouvez leur signaler de cette façon:

# Élaboration de la demande

Rendez vous sur [dev.getsol.us](https//dev.getsol.us/) puis cliquez sur **Report a new issue**. La demande devra se faire **en anglais**.

À la case _title_, indiquez le logiciel et la version, par exemple: **Update Firefox to 68.1**

Dans la description, indiquez en détail ce qu'apporte cette nouvelle version. N'hésitez pas à vous rendre sur le site du projet pour copier les nouveauté, voire même fouiller les source à la recherche d'un fichier CHANGELOG plus explicite. Plus vous donnez d'informations, plus votre demande aura du poids. Imaginez que la nouvelle version corrige des failles de sécurité...

Enfin, cliquez sur **Create New Task**.
