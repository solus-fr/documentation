# Premier packaging

---

Après avoir suivi [les bases](bases.md) ainsi qu'avoir découvert le format [`package.yml`](format_packageyml.md) il est temps de construire notre premier paquet.

Ce qui va suivre par d'un empaquetage simple, selon les logiciels empaquetés les étapes peuvent variées. Les erreurs de construction sont volontairement inclues dans le guide pour montrer plus clairement les étapes.

## Mise en place et récupération des sources

Dans un premier temps créons un dossier appelé `simple-scan` à la racine de notre dossier `Solbuild` comportant l'ensemble de scripts `common`.

![Dossier de construction](images/premier_paquet/dossier_construction.png)

Une fois dans ce dossier, ouvrez une fenêtre de terminal pour inclure le fichier `Makefile.common` avec la commande `echo "include ../Makefile.common" > Makefile`.

![Makefile](images/premier_paquet/makefile.png)

Pour récupérer les sources nous allons appeler un script se situant dans `common` qui fera le travail à notre place pour calculer la somme de contrôle et structurer rapidement le fichier `package.yml`.  
Dans votre fenêtre de terminal tapez la commande `../common/Scripts/yauto.py https://download.gnome.org/sources/simple-scan/3.28/simple-scan-3.28.1.tar.xz`

!!! tip "Utilisez des alias"
    Afin de se simplifier la vie, vous pouvez ajouter des alias dans le fichier `~/.bashrc` comme par exemple
    ``` bash
    alias fetchYml="$HOME/Solbuild/common/Scripts/yauto.py"
    alias genMakefile="echo "include ../Makefile.common" > Makefile"
    ```
    Il vous suffit ensuite de taper le nom de l'alias au lieu de la commande, par exemple `fetchYml https://download.gnome.org/sources/simple-scan/3.28/simple-scan-3.28.1.tar.xz`

Normalement un fichier `package.yml` a été crée et si vous l'ouvrez vous devez retrouver ce qui est sur la capture d'écran si dessous.

![Récupération des sources](images/premier_paquet/recuperation_sources.png)

La base est donc là, il faut s'assurer du type de licence utilisé par le logiciel. La license est souvent documentée dans un fichier `LICENSE` ou `COPYING` et se trouve dans le répertoire racine de l'application. De plus les dépôt tels que [GitLab](https://gitlab.gnome.org/GNOME/simple-scan) `GNU GPLv3` ou [GitHub](https://github.com/GNOME/simple-scan) `GPL-3.0` mentionnent très souvent la license utilisée. 
Nous voyons que `simple-scan` utilise une license GPL en version 3.0, il faut l'indiquer dans le paquet en utilisant le code [SPDX verson 3.x](https://spdx.org/licenses/) correspondant à la license soit [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html) (et supprimer le `# CHECK ME` du coup).

Ensuite il faut classer le logiciel dans la partie `component`, prenons `office`.

!!! info
    La commande `eopkg lc` établie une liste des catégories disponibles.

Il faut alors remplir `summary` et `descriptions`.

  - Le premier peut contenir quelquchose de suscint comme :
  > a GNOME document scanning application.
  - Le deuxième est plus détaillé, allons-y pour :
  > Simple Scan is an easy-to-use application, designed to let users connect their scanner and quickly have the image/document in an appropriate format.
  
Bien lançons la construction avec `make` pour voir. Vous devez obtenir une erreur.

```
[YAML:builddeps] Mandatory token cannot be empty
ERRO[09:45:52] Failed to install build dependencies          buildFile="/home/build/work/package.yml" error="exit status 1"
ERRO[09:45:56] Failed to build packages 
```

La construction ne peut avoir lieu car aucunes dépendances pour la construction ne sont indiquées alors que le paramètre est là. Il est possible de le supprimer et de relancer `make`.

Nouveau soucis, il n'y a apparemment rien à configurer avec `%configure`, en fait il s'agit d'un problème de *macros*. **Ce** logiciel ne se construit pas avec les outils que son `configure/make/make install` mais avec `meson`. Il faut donc adapter notre fichier `package.yml` pour indiquer les bonnes macros à utiliser.

```
setup      : |
    %meson_configure
build      : |
    %ninja_build
install    : |
    %ninja_install
```

## Les dépendances de construction

La construction utilise maintenant les bonnes `%macros`, pas de raisons que cela ne marche pas, c'est reparti.

`meson.build:1:0: ERROR:  Could not execute Vala compiler "valac"`

Impossible de trouver le compilateur Vala, tout simplement parce-qu’il n'est pas dans l'environnement de construction. il suffit de le rajouter et c'est reparti.

```
builddeps   :
    - vala
```

```
meson.build:21:0: ERROR:  Dependency "gtk+-3.0" not found, tried pkgconfig
```

Encore une erreur. Bon ça commence à être embêtant car s'il y a un gros nombre de dépendances la construction va prendre des années...


Deux solutions s'offrent à vous :

  - Soit vous scrutez ce que vous dit votre terminal pour y trouver les informations sur les dépendances manquantes. Ici :

```
Dependency glib-2.0 found: YES 2.56.3
Dependency gtk+-3.0 found: YES 3.22.30
Dependency gmodule-export-2.0 found: YES 2.56.3
Dependency gthread-2.0 found: YES 2.56.3
Dependency zlib found: YES 1.2.11
Dependency cairo found: YES 1.16.0
Dependency gdk-pixbuf-2.0 found: YES 2.38.0
Dependency gusb found: NO (tried pkgconfig)
```
  
  - Soit vous fouillez un peu les dépôts des logiciels, ici par exemple on retrouve rapidement [la liste des dépendances](https://gitlab.gnome.org/GNOME/simple-scan/blob/master/meson.build#L19).
  
Il est alors possible de rajouter l'ensemble des dépendances nécessaires dans `builddeps` pour obtenir quelque-chose comme cela

``` yaml
builddeps :
    - pkgconfig(colord)
    - pkgconfig(gtk+-3.0)
    - pkgconfig(gusb)
    - pkgconfig(libturbojpeg)
    - pkgconfig(sane-backends)l
    - itstool
    - vala
```

!!! info
    Pour vérifier qu'un paquet est présent dans les dépôts solus, utilisez `eopkg search <nom>`, et si vous souhaitez avoir des informations sur le paquet `eopkg info <nom>`.

Quand cela est possible utilisez les `pkgconfig` en priorités (ces dernier peuvent être vu avec un `eopkg info <nom>-devel`. Puis les paquets `-devel` et enfin les paquets normaux.

!!! info
    L'ordre doit être **alphabétique**, pour plus de clarté.
    
Le plus dur est fait, il suffit de relancer la construction pour obtenir un beau paquet tout beau.

![Paquet construit](images/premier_paquet/paquet_construit.png)







