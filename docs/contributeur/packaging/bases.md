# Les bases

---

Avant toute chose il est important d'installer les paquets nécessaires à la mise en place de l'outil de construction **Solbuild**.

!!! tip "Astuce"
    Cet environnement peut être installé dans une machine virtuelle (par exemple **VirtualBox**)

## Le fichier packager

Un fichier nommé `packager` doit se trouver dans `$HOME/.solus`. Il contient un nom et un adresse mail afin de pouvoir vous identifier lors de la construction d'un paquet.

```
[Packager]
Name=Votre nom
Email=votre.adresse@mail.truc
```
## Les outils de développement

Il est nécessaire, pour la construction, d'installer quelques logiciels permettant de récupérer, configurer et construire des paquets.

La commande `sudo eopkg install -c system.devel` vous permettra d'installer l'ensemble des logiciels nécessaires à la construction. `git` doit également être installé sur votre OS pour pouvoir continuer.

## Solbuild

Il faut ensuite installer *Solbuild* le système de construction et de packaging de Solus. la commande `sudo eopkg install solbuild` fera l'affaire.

Ensuite il y a deux possibilités pour utiliser *Solbuild*:

  - Utiliser la branche `shannon` (nom de la branche stable) et ainsi pouvoir installer les paquets construits directement sur son OS.
  - Utiliser la branche `unstable` et bénéficier de logiciels plus récents.

Si vous choisissez de construire avec `unstable` il vous faudra installer le paquet `solbuild-config-unstable`.

!!! info
    Les paquets qui sont envoyés à Solus pour évaluation **doivent** être construits depuis la branche `unstable` 

Une fois cela fait la commande `sudo solbuild init` peut être lancé et l’image de base de solbuild va être construite.

Solbuild est une image à un moment t des logiciels constituant la base du système de construction de paquets et donc, les versions des logiciels peuvent changer. Ainsi il est conseiller de mettre à jour solbuild régulièrement pour ne pas devoir à chaque construction télécharger les dites-mises à jour. La commande `sudo solbuild update` fera le travail.

## Les scripts Common

Récupérez `common` un ensemble de scripts aidant au packaging avec `git clone https://dev.getsol.us/source/common.git`.  
`common` doit être placé dans le dossier où vous allez construire vos paquets (voir exemple un peu plus loin).

Une fois récupéré il ne reste plus qu'à établir des liens symboliques

```bash
ln -sv common/Makefile.common .
ln -sv common/Makefile.toplevel Makefile
ln -sv common/Makefile.iso .
```

## Et ensuite ?

Si vous êtes arrivé ici les bases sont en places et la structure de votre dossier de construction devrait ressembler à cela.

```
| ~/
|   Solbuild/
|     common/
|       your-package/
|         - Makefile
|   Makefile
|   Makefile.common
|   Makefile.iso
```

Si c'est le cas, félicitation et vous pouvez passez à la prochaine étape.
