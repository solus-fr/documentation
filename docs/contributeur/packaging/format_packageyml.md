# Le format package.yml

---

Pour la construction de paquet **Solus** repose sur une infrastructure *maison* nommé **ypkg**. Le fichier essentiel à toute construction se nomme `package.yml` il centralise toutes les informations dont *Solbuild* aura besoin pour construire le paquet.

Regardons alors comment se structure ce fichier avec un exemple simple, celui du client torrent `transmission`.

```
name       : transmission
version    : '2.94'
release    : 10
source     :
    - https://github.com/transmission/transmission-releases/raw/master/transmission-2.94.tar.xz : 35442cc849f91f8df982c3d0d479d650c6ca19310a994eccdaa79a4af3916b7d
license    : GPL-2.0-or-later
component  : network.download
summary    : Lightweight BitTorrent client
description: |
    Transmission provides a simple and easy to use cross-platform lightweight BitTorrent client
builddeps  :
    - pkgconfig(gtk+-3.0)
    - pkgconfig(libcurl)
    - pkgconfig(libevent)
setup      : |
    %configure --with-gtk
build      : |
    %make
install    : |
    %make_install
check      : |
    %make check
```

## Structure du fichier

  - `name` (en minuscule) indique le nom du paquet (le nom du logiciel ou de la librairie)

??? tip "languages de programmation"
    Pour Haskell, Perl, Python & Ruby, il est d'usage de préfixer le nom du paquet par le nom du language de programmation. Par exemple `python-dbus` ou `perl-sdl`.

  - `version` indique le numéro de la version empaquetée
  - `release` indique le nombres de publications, ainsi ici le logiciel a eu 10 publications dans les dépôts. Cela peut être pour des montées de versions, des changements dans les dépendances, dans la façon de construire le paquet ...

!!! tip "Astuce"
    Ainsi pour qu'une publication soit disponible au utilisateur, le numéro doit être incrémenté de 1. `make bump` permet d'incrémenter le numéro de publication d'un paquet.
  
  - `source` comporte un format du type `url : somme de contrôle`, ceci détermine où les sources doivent être téléchargées ainsi que le hache de l'archive pour vérifier qu'il s'agit bien de la même archive.

??? info
    - Préférez les formats d'archive `tar.*` au format `.zip` lorsque c'est possible
    - La somme de contrôle est au format SHA-256, la somme d'un fichier peut être contrôlé via la ligne de commande avec `sha256sum <nom du fichier>`
    - Si votre paquet comporte plus qu'une source, vous devez ajouter les sources supplémentaires manuellement

  - `license` (identifiant [SPDX](https://spdx.org/licenses/) 3.x) spécifie le(s) type(s) de licence utilisé

??? info "Informations avancées"
    - Certaines licences telles que la GPL existent avec un suffix `-only` ou `-or-later`, ce qui signifie par exemple uniquement en version GPL 2.0 `GPL-2.0-only` ou en version GPL 2.0 et versions ultérieures `GPL-2.0-or-later`. Pour ce type de licence, il n'est pas toujours simple de déterminer quel suffixe utiliser. Notre conseil est de faire une recherche sur `or later` dans le texte de la licence. Si vous ne trouvez rien, le suffixe est probablement `-only`. En cas de doute préférez toujours `-or-later`
    - Certains logiciels donnent le choix entre plusieurs licences (par exemple MPL-1.1 **OU** Apache-2.0). Dans ce cas-là, vous ne devez indiquer qu'une des licences. D'autres utilisent plusieurs licences (**ET**), dans ce cas, il faut toutes les indiquer. Il est possible de combiner les **ET** et les **OU**
    - Il arrive (rarement) qu'une licence soit utilisée avec [des exceptions](https://spdx.org/licenses/exceptions-index.html), il faut alors combiner les identifiants de la licence et des exceptions avec le mot-clé **WITH**. C'est le cas notamment du paquet [wxwidgets](https://dev.getsol.us/source/wxwidgets/browse/master/package.yml$6) dont l'identifiant de licence est `LGPL-2.0-or-later WITH WxWindows-exception-3.1`
    - Les identifiants des licences doivent être triés par ordre alphabétique

  - `component` se veut être la catégorie correspondant au logiciel.

!!! tip "Astuce"
    `eopkg lc` permet de lister les catégories disponibles

  - `summary` décrit brièvement le logiciel
  - `description` apporte un résumé du logiciel, plus détaillé que `summary`.

!!! info
    Même si lorsque la description ne comporte qu'une seule ligne, il est d'usage de la formatter comme un bloc yaml, c'est pour cela que `description:` est toujours suivi d'un `|` et que le texte indenté commence à la ligne suivante.

  - `builddeps` comporte les librairies/logiciels nécessaires à la construction du paquet. Dans ce cas, `transmission` nécessite d'avoir `gtk+-3.0`, `libcurl` et `libevent` pour que la construction puisse avoir lieu.

  - `rundeps` spécifie les librairies/logiciels nécessaire pour utiliser le paquet.

!!! info
    N'oubliez pas de trier les dépendances par ordre alphabétique

  - `setup` permet, avant de compiler les sources, d'appliquer une configuration spécifique, appliquer des patches ... Ici par exemple la commande `%configure` a un argument `--with-gtk`, ce dernier spécifie que le paquet doit être construit avec `gtk` dans le but d'avoir l'interface graphique
  - `build` section ou la commande de construction est exécutée (ici `%make`)
  - `install` installation dans la structure du paquet a lieu (ici avec `%make_install`)
  - `check` pour les tests unitaires (dans notre exemple `%make check`)

??? info
    - Si les tests nécessitent des dépendances supplémentaires, celle-ci doivent être ajoutées dans les `builddeps`
    - Ces tests sont une sécurité supplémentaires mais ne dispensent pas le packager de vérifier que le paquet fonctionne correctement
